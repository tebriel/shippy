from shippy.connectors import usps


def test_build_request():
    usps.USPS_USER_ID = "test_user_id"
    req = usps.build_request("tracking_id")
    assert req == ""
