import xml.etree.ElementTree as ET

from os import getenv

import requests

USPS_USER_ID = getenv("USPS_USER_ID", "")
USPS_USER_PASSWORD = getenv("USPS_USER_PASSWORD")
"""
https://secure.shippingapis.com/shippingapi.dll?API=TrackV2&XML=
<TrackRequest USERID="XXXXXXXXXXXX">
  <TrackID ID="XXXXXXXXXXXXXX"/>
</TrackRequest>
"""


def build_request(tracking_id: str) -> str:
    """Build The request structure for USPS"""
    creds = {"USERID": USPS_USER_ID}
    if USPS_USER_PASSWORD:
        creds["PASSWORD"] = USPS_USER_PASSWORD
    body = ET.Element("TrackRequest", attrib=creds)
    body.append(ET.Element("TrackID", attrib={"ID": tracking_id}))
    return ET.tostring(body)


def track_request(tracking_id: str) -> str:
    """Track a request from the USPS API"""
    resp = requests.get(
        "https://secure.shippingapis.com/shippingapi.dll",
        params={
            "API": "TrackV2",
            "XML": build_request(tracking_id),
        },
    )
    resp.raise_for_status()
